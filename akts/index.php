<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
?>
<input type="hidden" id="path" value="<?=SITE_TEMPLATE_PATH?>"
    <div class="filter-inner">
        <form action="" method="post" id="filter" class="form-inline" role="form">
            <div class="form-group">
                <input type="text" class="form-control" name="to" placeholder="от">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="from" placeholder="до">
            </div>
            <div class="form-group">
                <select id="company-list" name="company" class="form-control">
                    <option value="">Выберите компанию</option>
                </select>
            </div>
            <div class="checkbox">
                <label><input type="checkbox" value="" id="stamp">Печать</label>
            </div>
            <button type="submit" class="btn btn-primary">Показать</button>
        </form>
    </div>
<br>
    <div id="grid"></div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>