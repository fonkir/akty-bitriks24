<?php
$GLOBALS['iCompanies'] = 5;
$GLOBALS['iChats'] = 8;
$GLOBALS['iCompanyFunctions'] = 7;
$GLOBALS['iBotFunctions'] = 6;

class BitrixClass
{
    public static function getCompany($id = false)
    {
        $name = $GLOBALS['DOMAIN'];
        CModule::IncludeModule('iblock');

        $arFilter = Array('IBLOCK_ID' => $GLOBALS['iCompanies'], 'ACTIVE' => 'Y', 'NAME' => $name);
        if($id){
            $arFilter = Array('IBLOCK_ID' => $GLOBALS['iCompanies'], 'ACTIVE' => 'Y', 'ID' => $id);
        }

        $res = CIBlockElement::GetList(array(), $arFilter, false, array(), array('*'));
        while ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            $arProp = $ob->GetProperties();
            $result = array(
                'ELEM' => $arFields,
                'PROP' => $arProp
            );
        }

        //если есть домен и нет компании то создаем ее
        if(!isset($result) && isset($GLOBALS['DOMAIN'])){
            //если есть компания то выдаем
            // если нет то создаем
            $result = self::addCompany($name);
        }
        
        if($result){
            //обновляем активность
            self::updateCompanyActivity($result['ELEM']['ID']);
        }
        return $result;
    }
    
    //получить список компаний
    public static function getCompanyList()
    {
        CModule::IncludeModule('iblock');

        $arFilter = Array('IBLOCK_ID' => $GLOBALS['iCompanies'], 'ACTIVE' => 'Y');

        $result = array(); 
        $res = CIBlockElement::GetList(array(), $arFilter, false, array(), array('*'));
        while ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            $arProp = $ob->GetProperties();
            $result[] = array(
                'ELEM' => $arFields,
                'PROP' => $arProp
            );
        }

        return $result;
    }
    //получить список компаний у которых заканчивается token
    public static function getCompanyListRefreshExpires()
    {
        CModule::IncludeModule('iblock');

        $arFilter = Array('IBLOCK_ID' => $GLOBALS['iCompanies'], 'ACTIVE' => 'Y', '<PROPERTY_REFRESH_TOKEN_EXPIRES' => time());

        $result = array(); 
        $res = CIBlockElement::GetList(array(), $arFilter, false, array(), array('*'));
        while ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            $arProp = $ob->GetProperties();
            $result[] = array(
                'ELEM' => $arFields,
                'PROP' => $arProp
            );
        }

        return $result;
    }
    
    //получить список открытых чатов по компании
    public static function getChatListByCompany($id)
    {
        CModule::IncludeModule('iblock');

        $arFilter = Array('IBLOCK_ID' => $GLOBALS['iChats'], 'ACTIVE' => 'Y','PROPERTY_COMPANY'=>$id);

        $result = array(); 
        $res = CIBlockElement::GetList(array(), $arFilter, false, array(), array('*'));
        while ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            $arProp = $ob->GetProperties();
            $result[] = array(
                'ELEM' => $arFields,
                'PROP' => $arProp
            );
        }

        return $result;
    }

    //
    public static function getCompanyFunction($function_id, $company_id = false)
    {
        CModule::IncludeModule('iblock');

        if(!$company_id){
            $company = BitrixClass::getCompany();
            $company_id = $company['ELEM']['ID'];
        }

        $arFilter = Array('IBLOCK_ID' => $GLOBALS['iCompanyFunctions'],'PROPERTY_COMPANY' => $company_id, 'PROPERTY_FUNCTION' => $function_id);

        $res = CIBlockElement::GetList(array(), $arFilter, false, array(), array('*'));
        while ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            $arProp = $ob->GetProperties();
            $result = array(
                'ELEM' => $arFields,
                'PROP' => $arProp
            );
        }

        //если нет функции то создаем ее
        if(!isset($result)){
            $result = self::addCompanyFunction($function_id,$company_id);
            return $result;
        }

        return $result;
    }


    //Добавить функцию компании
    public static function addCompanyFunction($function_id,$company_id = false)
    {
        CModule::IncludeModule('iblock');

        if(!$company_id){
            $company = BitrixClass::getCompany();
            $company_id = $company['ELEM']['ID'];
        }

        $func_name = BitrixClass::getByID($function_id);
        self::addElement($GLOBALS['iCompanyFunctions'], $func_name , array('COMPANY'=>$company_id,'FUNCTION'=>$function_id));

        $result = self::getCompanyFunction($function_id,$company_id);
        if ($result) return $result;
    }

    //удалить функцию компании
    public static function deleteCompanyFunction($id)
    {
        CModule::IncludeModule('iblock');
        CIBlockElement::Delete($id);
    }

    //получаем список всех возможных функций бота
    public static function getMainFunctionsList()
    {
        CModule::IncludeModule('iblock');
        
        $result = array();

        $arFilter = Array('IBLOCK_ID' => $GLOBALS['iBotFunctions'], 'ACTIVE' => 'Y');

        $res = CIBlockElement::GetList(array(), $arFilter, false, array(), array('*'));
        while ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            $arProp = $ob->GetProperties();
            $result[] = array(
                'ELEM' => $arFields,
                'PROP' => $arProp
            );
        }

        return $result;
    }
    //получаем список функций которые не использованы в компании
    public static function getMainFunctionsListNoUsed($company_id = false)
    {
        CModule::IncludeModule('iblock');
        if(!$company_id){
            $company = BitrixClass::getCompany();
            $company_id = $company['ELEM']['ID'];
        }
        $listInCompany = self::getFunctionsListInCompany($company_id);

        function get_id_list($n)
        {
            return $n['PROP']['FUNCTION']['VALUE'];
        }
        $idList = array_map("get_id_list", $listInCompany);

        $result = array();

        $arFilter = Array('IBLOCK_ID' => $GLOBALS['iBotFunctions'], '!ID'=>$idList);

        $res = CIBlockElement::GetList(array(), $arFilter, false, array(), array('*'));
        while ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            $arProp = $ob->GetProperties();
            $result[] = array(
                'ELEM' => $arFields,
                'PROP' => $arProp
            );
        }

        return $result;
    }

    //получаем список всех активных функций бота у компании
    public static function getFunctionsListInCompany($company_id = false)
    {
        CModule::IncludeModule('iblock');
        
        if(!$company_id){
            $company = BitrixClass::getCompany();
            $company_id = $company['ELEM']['ID'];
        }
        $result = array();

        $arFilter = Array('IBLOCK_ID' => $GLOBALS['iCompanyFunctions'], 'ACTIVE' => 'Y', 'PROPERTY_COMPANY' => $company_id);

        $res = CIBlockElement::GetList(array(), $arFilter, false, array(), array('*'));
        while ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            $arProp = $ob->GetProperties();
            $result[] = array(
                'ELEM' => $arFields,
                'PROP' => $arProp
            );
        }

        return $result;
    }


    //устанавливаем время оповещения 
    public static function setFunctionNotifyTime($id,$time,$time_zone = false)
    {
        $interval = 'P1D';
        $company = BitrixClass::getCompany();
        if(!$time_zone){
            $time_zone = $company['PROP']['TIME_ZONE']['VALUE'];
        }
        CModule::IncludeModule('iblock');
        $ELEMENT_ID = $company['ELEM']['ID'];  // код элемента

        $PROPERTY_CODE = "TIME_ZONE";  // код свойства
        $PROPERTY_VALUE = $time_zone;  // значение свойства
        CIBlockElement::SetPropertyValues($ELEMENT_ID, $GLOBALS['iCompanies'], $PROPERTY_VALUE, $PROPERTY_CODE);


        CModule::IncludeModule('iblock');
        $ELEMENT_ID = $id;  // код элемента

        //установим время по времени компании
        $PROPERTY_CODE = "TIME";  // код свойства
        $PROPERTY_VALUE = $time;  // значение свойства
        CIBlockElement::SetPropertyValues($ELEMENT_ID, $GLOBALS['iCompanyFunctions'], $PROPERTY_VALUE, $PROPERTY_CODE);

        //установим время следующего выполнения с учетом времени GMT
        //Нужно проверить еще раз
        $PROPERTY_CODE = "DATETIME_NEXT";  // код свойства
        
        $dNow = new DateTime();
        $dTime = new DateTime();
        $timezoneServer = new DateTimeZone('+0300');
        $timezoneClient = new DateTimeZone($time_zone);

        list($hour, $minute) = explode(":", $time);
        $dTime->setTimezone($timezoneClient);
        $dTime->setTime($hour, $minute);

        $timeNowInZone = new DateTime();
        $timeNowInZone->setTimezone($timezoneClient);
        if($timeNowInZone > $dTime){
            $dTime->add(new DateInterval($interval));
        } 

        $dTime->setTimezone($timezoneServer);

        $PROPERTY_VALUE = $dTime->format('d.m.Y H:i:s');
        CIBlockElement::SetPropertyValues($ELEMENT_ID, $GLOBALS['iCompanyFunctions'], $PROPERTY_VALUE, $PROPERTY_CODE);
        
        //установим время последнего выполнения
        //Нужно проверить еще раз
        $PROPERTY_CODE = "DATETIME_LAST";  // код свойства
        $PROPERTY_VALUE = $dNow->format('d.m.Y H:i:s');
        CIBlockElement::SetPropertyValues($ELEMENT_ID, $GLOBALS['iCompanyFunctions'], $PROPERTY_VALUE, $PROPERTY_CODE);
    }

    public static function getListFunctionsForNotify()
    {
        $result = array();
        $dNow = new DateTime();

        $arFilter = Array('IBLOCK_ID' => $GLOBALS['iCompanyFunctions'], 'ACTIVE' => 'Y', '<PROPERTY_DATETIME_NEXT' => $dNow->format('Y-m-d H:i:s'));

        $res = CIBlockElement::GetList(array(), $arFilter, false, array(), array('*'));
        while ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            $arProp = $ob->GetProperties();
            $result[] = array(
                'ELEM' => $arFields,
                'PROP' => $arProp
            );
        }

        return $result;
    }

    //Добавить компанию
    public static function addCompany($name)
    {
        CModule::IncludeModule('iblock');
        self::addElement($GLOBALS['iCompanies'], $name, array('TIME_ZONE'=>'+0300'));
        $result = self::getCompany();
        TelegramBot::notify('Добавилась новая компания: '. $name);
        if ($result) return $result;
    }

    //Добавить элемент в битрикс
    public static function addElement($iblock, $name, $props)
    {
        global $USER;
        if (!is_object($USER)) $USER = new CUser;

        $el = new CIBlockElement;

        $arLoadProductArray = Array(
            "MODIFIED_BY" => $USER->GetID(), // элемент изменен текущим пользователем
            "IBLOCK_ID" => $iblock,
            "PROPERTY_VALUES" => $props,
            "NAME" => $name,
            "ACTIVE" => "Y",            // активен
        );

        $el->Add($arLoadProductArray);
    }
    //Добавить элемент в битрикс
    public static function getByID($id)
    {
        $res = CIBlockElement::GetByID($id);
        if($ar_res = $res->GetNext())
          return $ar_res['NAME'];

        return false;
    }

    public static function updateCompanyActivity($id)
    {
        $ELEMENT_ID = $id;  // код элемента
        $PROPERTY_CODE = "LAST_DATE_ACTIVITY";  // код свойства
        $PROPERTY_VALUE = date_format(new DateTime(), 'd.m.Y H:i:s');  // значение свойства

        // Установим новое значение для данного свойства данного элемента
        $dbr = CIBlockElement::GetList(array(), array("=ID" => $ELEMENT_ID), false, false, array("ID", "IBLOCK_ID"));
        if ($dbr_arr = $dbr->Fetch()) {
            $IBLOCK_ID = $dbr_arr["IBLOCK_ID"];
            CIBlockElement::SetPropertyValues($ELEMENT_ID, $IBLOCK_ID, $PROPERTY_VALUE, $PROPERTY_CODE);
        }
    }

    public static function haveBotToken()
    {
        $company = self::getCompany();
        if($company['PROP']['TELEGRAM_TOKEN']['VALUE'] && $company['PROP']['TELEGRAM_TOKEN']['VALUE'] !== ''){
            return array('result' => 'ok', 'TOKEN' => $company['PROP']['TELEGRAM_TOKEN']['VALUE']);
        }
        else
            return array('result' => 'ok', 'TOKEN' => '');
    }

    public static function addBotToken($val)
    {
        $company = self::getCompany();
        $ELEMENT_ID = $company['ELEM']['ID'];  // код элемента
        $PROPERTY_CODE = "TELEGRAM_TOKEN";  // код свойства
        $PROPERTY_VALUE = $val;

        CIBlockElement::SetPropertyValues($ELEMENT_ID, $GLOBALS['iCompanies'], $PROPERTY_VALUE, $PROPERTY_CODE);
        TelegramBot::setWebHook($val, $ELEMENT_ID);
        TelegramBot::notify("Компания {$company['ELEM']['NAME']} добавила токен чата: {$val}");
        return array('result' => true);
    }

    public static function clearBotToken()
    {
        return self::addBotToken('');
    }

    public static function addOpenChatBot($id,$company_id)
    {
        if(!self::checkOpenChat($id,$company_id)){
            self::addElement($GLOBALS['iChats'], $id, array('COMPANY'=>$company_id));
            TelegramBot::notify('Добавлен новый чат с ботом для компании с ID: '. $company_id);
        }
    }
    
    public static function checkOpenChat($id,$company)
    {
        CModule::IncludeModule('iblock');

        $arFilter = Array('IBLOCK_ID' => $GLOBALS['iChats'], 'ACTIVE' => 'Y', 'NAME' => $id, 'PROPERTY_COMPANY' => $company);
        
        $res = CIBlockElement::GetList(array(), $arFilter, false, array(), array('*'));
        while ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            
        }

        if(isset($arFields)){
            return true;
        }

        return false;
    }

    public static function hasTokenAuth()
    {
        $company = BitrixClass::getCompany();

        if($company['PROP']['AUTH_ID']['VALUE'] && $company['PROP']['REFRESH_TOKEN_EXPIRES']['VALUE'] > time()){
            return true;
        }

        return false;
    }

    public static function getTokenAuth($company)
    {
        if($company['PROP']['AUTH_ID']['VALUE'] && $company['PROP']['REFRESH_TOKEN_EXPIRES']['VALUE'] < time()){
            return $company['PROP']['AUTH_ID']['VALUE'];
        }
        else{
            return AppClass::setGetToken($company['ELEM']['ID'],$company['PROP']['REFRESH_ID']['VALUE']);
        }
    }


    public static function setRefreshToken($company_id,$access_token,$refresh_token,$expires_auth)
    {
        CModule::IncludeModule('iblock');
        $ELEMENT_ID = $company_id;  // код элемента

        //установим
        $PROPERTY_CODE = "AUTH_ID";  // код свойства
        $PROPERTY_VALUE = $access_token;  // значение свойства
        CIBlockElement::SetPropertyValues($ELEMENT_ID, $GLOBALS['iCompanies'], $PROPERTY_VALUE, $PROPERTY_CODE);
        //установим
        $PROPERTY_CODE = "REFRESH_ID";  // код свойства
        $PROPERTY_VALUE = $refresh_token;  // значение свойства
        CIBlockElement::SetPropertyValues($ELEMENT_ID, $GLOBALS['iCompanies'], $PROPERTY_VALUE, $PROPERTY_CODE);
        //установим
        $PROPERTY_CODE = "AUTH_TOKEN_EXPIRES";  // код свойства
        $PROPERTY_VALUE = $expires_auth;  // значение свойства
        CIBlockElement::SetPropertyValues($ELEMENT_ID, $GLOBALS['iCompanies'], $PROPERTY_VALUE, $PROPERTY_CODE);
        //установим
        $PROPERTY_CODE = "REFRESH_TOKEN_EXPIRES";  // код свойства
        $PROPERTY_VALUE = time()+2400000;  // значение свойства
        CIBlockElement::SetPropertyValues($ELEMENT_ID, $GLOBALS['iCompanies'], $PROPERTY_VALUE, $PROPERTY_CODE);
    }

    public static function setAuthTokens($access_token,$refresh_token)
    {
        CModule::IncludeModule('iblock');

        $company = BitrixClass::getCompany();
        $company_id = $company['ELEM']['ID'];

        $ELEMENT_ID = $company_id;  // код элемента
        //установим
        $PROPERTY_CODE = "AUTH_ID";  // код свойства
        $PROPERTY_VALUE = $access_token;  // значение свойства
        CIBlockElement::SetPropertyValues($ELEMENT_ID, $GLOBALS['iCompanies'], $PROPERTY_VALUE, $PROPERTY_CODE);
        //установим
        $PROPERTY_CODE = "REFRESH_ID";  // код свойства
        $PROPERTY_VALUE = $refresh_token;  // значение свойства
        CIBlockElement::SetPropertyValues($ELEMENT_ID, $GLOBALS['iCompanies'], $PROPERTY_VALUE, $PROPERTY_CODE);
        //установим
        $PROPERTY_CODE = "AUTH_TOKEN_EXPIRES";  // код свойства
        $PROPERTY_VALUE = time() + 3500;  // значение свойства
        CIBlockElement::SetPropertyValues($ELEMENT_ID, $GLOBALS['iCompanies'], $PROPERTY_VALUE, $PROPERTY_CODE);
        //установим
        $PROPERTY_CODE = "REFRESH_TOKEN_EXPIRES";  // код свойства
        $PROPERTY_VALUE = time()+2400000;  // значение свойства
        CIBlockElement::SetPropertyValues($ELEMENT_ID, $GLOBALS['iCompanies'], $PROPERTY_VALUE, $PROPERTY_CODE);
    }
    public static function setDepartToNotify($id,$depart)
    {
        CModule::IncludeModule('iblock');
        $company = BitrixClass::getCompany();
        $company_id = $company['ELEM']['ID'];

        $ELEMENT_ID = $id;  // код элемента

        $PROPERTY_CODE = "DEPART_PARAM";  // код свойства
        $PROPERTY_VALUE = $depart;  // значение свойства
        CIBlockElement::SetPropertyValues($ELEMENT_ID, $GLOBALS['iCompanyFunctions'], $PROPERTY_VALUE, $PROPERTY_CODE);


        $ELEMENT_ID = $company_id;  // код элемента

        $PROPERTY_CODE = "DEPART";  // код свойства
        $PROPERTY_VALUE = $depart;  // значение свойства
        CIBlockElement::SetPropertyValues($ELEMENT_ID, $GLOBALS['iCompanies'], $PROPERTY_VALUE, $PROPERTY_CODE);
    }

}