<?php
/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 16.04.2018
 * Time: 0:26
 */

class B24Class
{
    public static function restCommand($method, array $params = Array(),$domain = '', $auth = '', $authRefresh = true)
    {
        $queryUrl = "https://".$domain."/rest/".$method;
        $queryData = http_build_query(array_merge($params, array("auth" => $auth)));

        writeToLog(Array('URL' => $queryUrl, 'PARAMS' => array_merge($params, array("auth" => $auth))), 'ImBot send data');

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_POST => 1,
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_SSL_VERIFYPEER => 1,
            CURLOPT_URL => $queryUrl,
            CURLOPT_POSTFIELDS => $queryData,
        ));

        $result = curl_exec($curl);
        curl_close($curl);

        $result = json_decode($result, 1);

        if ($authRefresh && isset($result['error']) && in_array($result['error'], array('expired_token', 'invalid_token')))
        {
            $auth = restAuth($auth);
            if ($auth)
            {
                $result = restCommand($method, $params, $auth, false);
            }
        }

        return $result;
    }
}