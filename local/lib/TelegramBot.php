<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 14.03.2018
 * Time: 13:24
 */

class TelegramBot
{
    public static function sendMessage($msg,$token, $chat_id){
        curl_get_contents("https://api.telegram.org/bot{$token}/sendMessage?chat_id={$chat_id}&parse_mode=html&text={$msg}");
    }

    public static function getBotClients($company){
        $token = $company['PROP']['TELEGRAM_TOKEN']['VALUE']; // тут вводим ваш токен;
        $url = "https://api.telegram.org/bot{$token}/getUpdates";
        $res = curl_get_contents($url);
        $arRes = json_decode($res, true);
        return $arRes;
    }

    public static function setWebHook($token,$company_id){
        $url = "https://api.telegram.org/bot{$token}/setWebhook?url=https://app.kolibbri.ru/bitrix24_bot/app/bothook.php?id={$company_id}";
        $res = curl_get_contents($url);
    }

    public static function getBotInfo(){
        $company = BitrixClass::getCompany(); 
        $token = $company['PROP']['TELEGRAM_TOKEN']['VALUE'];
        $url = "https://api.telegram.org/bot{$token}/getMe";
        $res = curl_get_contents($url);
        $arRes = json_decode($res, true);
        return $arRes;
    }
    public static function getWebHookInfo($token){
        $url = "https://api.telegram.org/bot{$token}/getWebhookInfo";
        $res = curl_get_contents($url);
        return $res;
    }

    //notify
    public static function notify($msg){
        $token = '568751290:AAGWfYin7NiPJ92kwf4tTmV9xrtOvsrJ_Yc';
        self::sendMessage(urlencode($msg),$token,'-271298091');

    }
    /*сообщение по крону
     *собираем список копаний
     *собираем список чатов по копании
     *отсылаем нужное сообщение*/
    public static function sendCronMessage(){


        $functionListForNotify = BitrixClass::getListFunctionsForNotify();
        foreach ($functionListForNotify as $function) {

            $company = BitrixClass::getCompany($function['PROP']['COMPANY']['VALUE']);
            $company['PROP']['AUTH_ID']['VALUE'] = BitrixClass::getTokenAuth($company);
            $company_id = $company['ELEM']['ID'];
            $token = $company['PROP']['TELEGRAM_TOKEN']['VALUE'];

            $chat_list = BitrixClass::getChatListByCompany($company_id);

            $domain = $company['ELEM']['NAME'];
            $auth = BitrixClass::getTokenAuth($company);
            $checkAuth = Bitrix24Class::checkAdmin($domain,$auth,'Y');
            if($checkAuth['error'] == 'NO_AUTH_FOUND'){
                foreach ($chat_list as $chat) {
                    $chat_id = $chat['ELEM']['NAME'];
                    //pre($chat_id.' '.$token);
                    self::sendMessage(urlencode('Ваш бот неавторизован, пожалуйста, зайдите в настройки бота в своем Битрикс24.'), $token, $chat_id);
                }
            }
            else{
                $timeNow = new DateTime();
                $timezoneClient = new DateTimeZone($company['PROP']['TIME_ZONE']['VALUE']);
                $timeNow->setTimezone($timezoneClient);

                $msg = "<b>".$function['ELEM']['NAME']." за ".$timeNow->format('d.m.Y')."</b>\n\n";
                switch ($function['ELEM']['NAME']){
                    case 'Общий отчет':
                        $msg .= Bitrix24Class::getMsgByTemplate($company,'base');
                        break;
                    case 'Отчет по лидам':
                        $msg .= Bitrix24Class::getMsgByTemplate($company,'leads');
                        break;
                    case 'Отчет по сделкам':
                        $msg .= Bitrix24Class::getMsgByTemplate($company,'deals');
                        break;
                    case 'Отчет по продажам':
                        $msg .= Bitrix24Class::getMsgByTemplate($company,'sales');
                        break;
                    case 'Отчет по менеджерам':
                        $msg .= Bitrix24Class::getMsgByTemplate($company,'manager',array('DEPART'=>$function['PROP']['DEPART_PARAM']['VALUE']));
                        break;
                }
                $msg = urlencode($msg);
                foreach ($chat_list as $chat) {
                    $chat_id = $chat['ELEM']['NAME'];
                    //pre($chat_id.' '.$token);
                    self::sendMessage($msg, $token, $chat_id);
                }

            }

            BitrixClass::setFunctionNotifyTime(
                $function['ELEM']['ID'],
                $function['PROP']['TIME']['VALUE'],
                $company['PROP']['TIME_ZONE']['VALUE']
            );

        }

        /*$companies = BitrixClass::getCompanyList();

        foreach ($companies as $company) {
            $token = $company['PROP']['TELEGRAM_TOKEN']['VALUE'];
            $company_id = $company['ELEM']['ID'];

            //Узнаем сколько сейчас времени в компании с учетом временной зоны в формате Y-m-d H:i:s
            $company_time = KUtils::getDatetimeWithZone($company['PROP']['TIMEZONE']['VALUE']);    

            //теперь смотрим все активные функции компании
            //выбираем те, которые нужно разослать где следущее время рассылки < текущего времени
            

            $chat_list = BitrixClass::getChatListByCompany($company_id);  

            foreach ($chat_list as $chat) {
                $chat_id = $chat['ELEM']['NAME'];
                //pre($chat_id.' '.$token);  
                self::sendMessage($token, $chat_id);
            }
        }*/
    }
}