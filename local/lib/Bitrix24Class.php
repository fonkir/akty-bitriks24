<?php

//https://dev.1c-bitrix.ru/rest_help/general/batch.php
class Bitrix24Class
{

    public static function getDataFromBitrix24($company, $type, $param = array())
    {
        $domain = $company['ELEM']['NAME'];
        $auth = $company['PROP']['AUTH_ID']['VALUE'];
        $data = array();
        switch ($type) {
            case 'today_leads':
                //$data
                //Получаем список лидов
                $dTime1 = new DateTime();
                $dTime2 = new DateTime();
                $timezoneClient = new DateTimeZone($company['PROP']['TIME_ZONE']['VALUE']);
                $dTime1->setTimezone($timezoneClient);
                $dTime2->setTimezone($timezoneClient);
                $dTime2->modify('1 day');
                //$dTime1->add(new DateInterval('-P1D'));

                $filter = array(
                    ">DATE_CREATE" => $dTime1->format('Y-m-d') . 'T00:00:00' . $company['PROP']['TIME_ZONE']['VALUE'],//.$dTime1->format('H:i:s')
                    "<DATE_CREATE" => $dTime2->format('Y-m-d') . 'T00:00:00' . $company['PROP']['TIME_ZONE']['VALUE']//.$dTime2->format('H:i:s')
                );
                return self::b_getLeadList($domain, $auth, $filter)['result'];
                break;
            case 'all_leads_in_work':
                $filter = array(
                    "!STATUS_ID" => array('JUNK', 'CONVERTED', 'NEW')
                );
                return self::b_getLeadList($domain, $auth, $filter);
                break;
            case 'leads_junk_today':
                //$data
                //Получаем список лидов
                $dTime1 = new DateTime();
                $dTime2 = new DateTime();
                $timezoneClient = new DateTimeZone($company['PROP']['TIME_ZONE']['VALUE']);
                $dTime1->setTimezone($timezoneClient);
                $dTime2->setTimezone($timezoneClient);
                $dTime2->modify('1 day');
                //$dTime1->add(new DateInterval('-P1D'));

                $filter = array(
                    "STATUS_ID" => array('JUNK'),
                    ">=DATE_CLOSED" => $dTime1->format('Y-m-d') . 'T00:00:00' . $company['PROP']['TIME_ZONE']['VALUE'],//.$dTime1->format('H:i:s')
                    "<=DATE_CLOSED" => $dTime2->format('Y-m-d') . 'T00:00:00' . $company['PROP']['TIME_ZONE']['VALUE']//.$dTime2->format('H:i:s')
                );
                return self::b_getLeadList($domain, $auth, $filter);
                break;
            case 'leads_converted_today':
                //$data
                //Получаем список лидов
                $dTime1 = new DateTime();
                $dTime2 = new DateTime();
                $timezoneClient = new DateTimeZone($company['PROP']['TIME_ZONE']['VALUE']);
                $dTime1->setTimezone($timezoneClient);
                $dTime2->setTimezone($timezoneClient);
                $dTime2->modify('1 day');
                //$dTime1->add(new DateInterval('-P1D'));

                $filter = array(
                    "STATUS_ID" => array('CONVERTED'),
                    ">=DATE_CLOSED" => $dTime1->format('Y-m-d') . 'T00:00:00' . $company['PROP']['TIME_ZONE']['VALUE'],//.$dTime1->format('H:i:s')
                    "<=DATE_CLOSED" => $dTime2->format('Y-m-d') . 'T00:00:00' . $company['PROP']['TIME_ZONE']['VALUE']//.$dTime2->format('H:i:s')
                );
                return self::b_getLeadList($domain, $auth, $filter);
                break;
            case 'all_leads_not_in_work':
                $filter = array(
                    "STATUS_ID" => array('NEW'),
                );
                return self::b_getLeadList($domain, $auth, $filter);
                break;
            case 'today_leads_modify':
                //$data
                //Получаем список лидов
                $dTime1 = new DateTime();
                $dTime2 = new DateTime();
                $timezoneClient = new DateTimeZone($company['PROP']['TIME_ZONE']['VALUE']);
                $dTime1->setTimezone($timezoneClient);
                $dTime2->setTimezone($timezoneClient);
                $dTime2->modify('1 day');
                //$dTime1->add(new DateInterval('-P1D'));

                $filter = array(
                    ">=DATE_MODIFY" => $dTime1->format('Y-m-d') . 'T00:00:00' . $company['PROP']['TIME_ZONE']['VALUE'],//.$dTime1->format('H:i:s')
                    "<=DATE_MODIFY" => $dTime2->format('Y-m-d') . 'T00:00:00' . $company['PROP']['TIME_ZONE']['VALUE']//.$dTime2->format('H:i:s')
                );
                return self::b_getLeadList($domain, $auth, $filter)['result'];
                break;
            case 'last_deal':
                //$data
                //Получаем список лидов
                $dTime1 = new DateTime();
                $dTime2 = new DateTime();
                $timezoneClient = new DateTimeZone($company['PROP']['TIME_ZONE']['VALUE']);
                $dTime1->setTimezone($timezoneClient);
                $dTime2->setTimezone($timezoneClient);
                $dTime2->modify('1 day');
                //$dTime1->add(new DateInterval('-P1D'));

                $filter = array(
                    ">DATE_CREATE" => $dTime1->format('Y-m-d') . 'T00:00:00' . $company['PROP']['TIME_ZONE']['VALUE'],//.$dTime1->format('H:i:s')
                    "<=DATE_CREATE" => $dTime2->format('Y-m-d') . 'T00:00:00' . $company['PROP']['TIME_ZONE']['VALUE'],//.$dTime2->format('H:i:s')
                );
                return self::b_getList($domain, $auth, 'deal', $filter)['result'];
                break;
            case 'won_deals':
                //$data
                //Получаем список лидов
                $dTime1 = new DateTime();
                $dTime2 = new DateTime();
                $timezoneClient = new DateTimeZone($company['PROP']['TIME_ZONE']['VALUE']);
                $dTime1->setTimezone($timezoneClient);
                $dTime2->setTimezone($timezoneClient);
                $dTime2->modify('1 day');
                //$dTime1->add(new DateInterval('-P1D'));

                $filter = array(
                    ">DATE_MODIFY" => $dTime1->format('Y-m-d') . 'T00:00:00' . $company['PROP']['TIME_ZONE']['VALUE'],//.$dTime1->format('H:i:s')
                    "<=DATE_MODIFY" => $dTime2->format('Y-m-d') . 'T00:00:00' . $company['PROP']['TIME_ZONE']['VALUE'],//.$dTime2->format('H:i:s')
                    "STAGE_ID" => array('WON')
                );
                return self::b_getList($domain, $auth, 'deal', $filter);
                break;
            case 'lose_deals':
                //$data
                //Получаем список лидов
                $dTime1 = new DateTime();
                $dTime2 = new DateTime();
                $timezoneClient = new DateTimeZone($company['PROP']['TIME_ZONE']['VALUE']);
                $dTime1->setTimezone($timezoneClient);
                $dTime2->setTimezone($timezoneClient);
                $dTime2->modify('1 day');
                //$dTime1->add(new DateInterval('-P1D'));

                $filter = array(
                    "STAGE_ID" => array('LOSE'),
                    ">=DATE_MODIFY" => $dTime1->format('Y-m-d') . 'T00:00:00' . $company['PROP']['TIME_ZONE']['VALUE'],//.$dTime1->format('H:i:s')
                    "<=DATE_MODIFY" => $dTime2->format('Y-m-d') . 'T00:00:00' . $company['PROP']['TIME_ZONE']['VALUE']//.$dTime2->format('H:i:s')
                );
                return self::b_getList($domain, $auth, 'deal', $filter);;
                break;
            case 'all_deal_not_won':
                //$data
                //Получаем список сделок не завершенные
                $filter = array(
                    "!STAGE_ID" => array('LOSE', 'WON', 'NEW')
                );
                return self::b_getList($domain, $auth, 'deal', $filter);
                break;
            case 'deals_not_in_work':
                //$data
                //Получаем список сделок не завершенные
                $filter = array(
                    "STAGE_ID" => array('NEW')
                );
                return self::b_getList($domain, $auth, 'deal', $filter);
                break;
            case 'last_invoice':
                //$data
                //Получаем список лидов
                $dTime1 = new DateTime();
                $dTime2 = new DateTime();
                $timezoneClient = new DateTimeZone($company['PROP']['TIME_ZONE']['VALUE']);
                $dTime1->setTimezone($timezoneClient);
                $dTime2->setTimezone($timezoneClient);
                $dTime2->modify('1 day');
                //$dTime1->add(new DateInterval('-P1D'));

                $filter = array(
                    ">=DATE_STATUS" => $dTime1->format('Y-m-d') . 'T00:00:00' . $company['PROP']['TIME_ZONE']['VALUE'],//.$dTime1->format('H:i:s')
                    "<=DATE_STATUS" => $dTime2->format('Y-m-d') . 'T00:00:00' . $company['PROP']['TIME_ZONE']['VALUE'],//.$dTime1->format('H:i:s')
                    "STATUS_ID" => 'P'
                );
                return self::b_getInvoiceList($domain, $auth, $filter);
                break;
            case 'invoice':
                //$data
                //Получаем список лидов
                $dTime1 = new DateTime();
                $dTime2 = new DateTime();
                $timezoneClient = new DateTimeZone($company['PROP']['TIME_ZONE']['VALUE']);
                $dTime1->setTimezone($timezoneClient);
                $dTime2->setTimezone($timezoneClient);
                $dTime2->modify('1 day');
                //$dTime1->add(new DateInterval('-P1D'));

                $filter = array(
                    ">=DATE_STATUS" => $dTime1->format('Y-m-d') . 'T00:00:00' . $company['PROP']['TIME_ZONE']['VALUE'],//.$dTime1->format('H:i:s')
                    "<=DATE_STATUS" => $dTime2->format('Y-m-d') . 'T00:00:00' . $company['PROP']['TIME_ZONE']['VALUE'],//.$dTime1->format('H:i:s')
                    "STATUS_ID" => 'P'
                );
                return self::b_getInvoiceList($domain, $auth, $filter);
                break;
            case 'last_invoice_no_closed':
                //$data
                //Получаем список лидов
                $dTime1 = new DateTime();
                $dTime2 = new DateTime();
                $timezoneClient = new DateTimeZone($company['PROP']['TIME_ZONE']['VALUE']);
                $dTime1->setTimezone($timezoneClient);
                $dTime2->setTimezone($timezoneClient);
                $dTime2->modify('1 day');
                //$dTime1->add(new DateInterval('-P1D'));

                $filter = array(
                    "<=DATE_STATUS" => $dTime2->format('Y-m-d') . 'T00:00:00' . $company['PROP']['TIME_ZONE']['VALUE'],//.$dTime1->format('H:i:s')
                    "!STATUS_ID" => 'P',
                );
                return self::b_getInvoiceList($domain, $auth, $filter);
                break;
            case 'last_invoice_pay_today':
                //$data
                //Получаем список лидов
                $dTime1 = new DateTime();
                $dTime2 = new DateTime();
                $timezoneClient = new DateTimeZone($company['PROP']['TIME_ZONE']['VALUE']);
                $dTime1->setTimezone($timezoneClient);
                $dTime2->setTimezone($timezoneClient);
                $dTime2->modify('1 day');
                //$dTime1->add(new DateInterval('-P1D'));
                $filter = array(
                    ">=DATE_PAY_BEFORE" => $dTime1->format('Y-m-d') . 'T00:00:00' . $company['PROP']['TIME_ZONE']['VALUE'],//.$dTime1->format('H:i:s')
                    "<=DATE_PAY_BEFORE" => $dTime2->format('Y-m-d') . 'T00:00:00' . $company['PROP']['TIME_ZONE']['VALUE'],//.$dTime1->format('H:i:s')
                    "!STATUS_ID" => 'P',
                );
                return self::b_getInvoiceList($domain, $auth, $filter);
                break;
            case 'get_fields':
                return self::b_getFields($domain, $auth, 'lead', array());
                break;
            case 'get_source_list':
                return self::b_getSourceList($domain, $auth);
                break;
            case 'get_status_list':
                return self::b_getStatusList($domain, $auth);
                break;
            case 'get_stage_list':
                return self::b_getStageList($domain, $auth);
                break;
            case 'get_departs_list':
                return self::b_getDepartsList($domain, $auth);
                break;
            case 'get_user_list':
                return self::b_getUserList($domain, $auth, $param);
                break;
        }

        return $data;
    }

    public static function getMsgByTemplate($company, $tmpl, $param = array())
    {
        $msg = '';
        switch ($tmpl) {
            case 'base':
                $leads = self::getDataFromBitrix24($company, 'today_leads');
                $msg = self::emoji('push') . " Кол-во лидов: " . count($leads) . "\n";

                $deals = self::getDataFromBitrix24($company, 'last_deal');
                $msg .= self::emoji('deal') . " Кол-во сделок: " . count($deals) . "\n";

                $convers = intval(count($deals) / count($leads) * 100);
                $msg .= self::emoji('check') . " Конверсия из лиды в сделки: " . number_format($convers, 0, '.', ' ') . "%\n";


                $deal_price = 0;
                $won_deals = self::getDataFromBitrix24($company, 'won_deals')['result'];
                foreach ($won_deals as $deal) {
                    $deal_price += intval($deal['OPPORTUNITY']);
                }
                $msg .= self::emoji('money') . " Прибыль по сделкам: " . number_format($deal_price, 0, '.', ' ') . " руб.\n";
                //прибыль по счетам
                $invoice_price = 0;
                $invoices = self::getDataFromBitrix24($company, 'last_invoice');
                foreach ($invoices as $invoice) {
                    $invoice_price += intval($invoice['PRICE']);
                }
                $msg .= self::emoji('money') . " Прибыль по счетам: " . number_format($invoice_price, 0, '.', ' ') . " руб.";
                break;
            case 'leads':
                $leads = self::getDataFromBitrix24($company, 'today_leads');
                $msg = self::emoji('push') . " Новые: " . count($leads) . "\n";
                $msg .= "Из них:\n";
                $num = 1;
                //собираем лиды по источникам
                $source_list = self::getDataFromBitrix24($company, 'get_source_list');
                foreach ($source_list as $source) {
                    $lead_count = 0;
                    foreach ($leads as $lead) {
                        if ($lead['SOURCE_ID'] === $source['STATUS_ID']) {
                            $lead_count++;
                        }
                    }
                    if ($lead_count > 0) {
                        $msg .= $num . ") {$source['NAME']}: {$lead_count} \n";
                        $num++;
                    }
                }
                $msg .= "\n";

                $msg .= "Изменены сегодня:\n";

                $count_converted_leads = self::getDataFromBitrix24($company, 'leads_converted_today')['total'];
                $msg .= self::emoji('check') . " Успешных: {$count_converted_leads} \n";

                $count_junk_leads = self::getDataFromBitrix24($company, 'leads_junk_today')['total'];
                $msg .= self::emoji('no-check') . " Неуспешных: {$count_junk_leads} \n\n";

                $count_new_leads = self::getDataFromBitrix24($company, 'all_leads_not_in_work')['total'];
                $msg .= self::emoji('alert') . " Необработанные: {$count_new_leads} \n";

                $count_not_in_work_lead = self::getDataFromBitrix24($company, 'all_leads_in_work')['total'];
                $msg .= self::emoji('clock') . " В работе: {$count_not_in_work_lead} \n";
                break;
            case 'deals':
                $deals = self::getDataFromBitrix24($company, 'last_deal');
                $msg = self::emoji('deal') . " Новые: " . count($deals) . "\n";

                $won_deals = self::getDataFromBitrix24($company, 'won_deals');
                $msg .= self::emoji('check') . " Успешных: {$won_deals['total']} \n";

                $lose_deals = self::getDataFromBitrix24($company, 'lose_deals')['total'];
                $msg .= self::emoji('no-check') . " Неуспешных: {$lose_deals} \n\n";

                $deals_not_in_work = self::getDataFromBitrix24($company, 'deals_not_in_work')['total'];
                $msg .= self::emoji('no-check') . " Необработанные: {$deals_not_in_work} \n";
                $deals_not_won = self::getDataFromBitrix24($company, 'all_deal_not_won')['total'];

                $msg .= self::emoji('alert') . " В работе: {$deals_not_won} \n";

                $deal_price = 0;
                foreach ($won_deals['result'] as $deal) {
                    $deal_price += intval($deal['OPPORTUNITY']);
                }
                $msg .= self::emoji('money') . " Прибыль: " . number_format($deal_price, 0, '.', ' ') . " руб.\n";

                /*$invoices = self::getDataFromBitrix24($company, 'last_invoice');
                $price = 0;
                foreach ($invoices as $invoice) {
                    $price += intval($invoice['PRICE']);
                }
                $msg .= self::emoji('money') . " Прибыль: " . number_format($price, 0, '.', ' ') . " руб.";*/
                break;
            case 'sales':
                $msg = '';

                $deal_price = 0;
                $won_deals = self::getDataFromBitrix24($company, 'won_deals')['result'];
                foreach ($won_deals as $deal) {
                    $deal_price += intval($deal['OPPORTUNITY']);
                }
                $msg .= self::emoji('money') . " Прибыль по сделкам: " . number_format($deal_price, 0, '.', ' ') . " руб.\n";
                //по счетам
                $invoices = self::getDataFromBitrix24($company, 'last_invoice');
                $price = 0;
                foreach ($invoices as $invoice) {
                    $price += intval($invoice['PRICE']);
                }
                $msg .= self::emoji('money') . " Прибыль по счетам: " . number_format($price, 0, '.', ' ') . " руб.\n";

                $invoices_no_closed = self::getDataFromBitrix24($company, 'last_invoice_no_closed');
                $price = 0;
                foreach ($invoices_no_closed as $invoice) {
                    $price += intval($invoice['PRICE']);
                }
                $msg .= self::emoji('alert') . " Долг: " . number_format($price, 0, '.', ' ') . " руб.\n";

                $invoices_pay_today = self::getDataFromBitrix24($company, 'last_invoice_pay_today');
                $price = 0;
                foreach ($invoices_pay_today as $invoice) {
                    $price += intval($invoice['PRICE']);
                }
                $msg .= self::emoji('time') . " Ожидаются сегодня: " . number_format($price, 0, '.', ' ') . " руб.";
                break;
            case 'manager':
                $msg = '';
                $msg .= self::emoji('top') . " Топ 3 за сегодня:\n";

                $manager_list = self::getDataFromBitrix24($company, 'get_user_list', array('UF_DEPARTMENT' => $param['DEPART']));
                $deals = self::getDataFromBitrix24($company, 'won_deals')['result'];
                //берем всех менеджеров и считаем суммы сделок
                foreach ($manager_list as $k => $manager) {
                    $manager['DEAL_SUM'] = 0;
                    $manager['DEALS'] = array();
                    foreach ($deals as $deal) {
                        if ($deal['ASSIGNED_BY_ID'] == $manager['ID']) {
                            $manager['DEALS'][] = $deal;
                            $manager['DEAL_SUM'] += intval($deal['OPPORTUNITY']);
                            $manager_list[$k] = $manager;
                        }
                    }
                }
                usort($manager_list, function ($a, $b) {
                    return -($a['DEAL_SUM'] - $b['DEAL_SUM']);
                });
                $num = 1;
                foreach ($manager_list as $manager) {
                    $countDeals = count($manager['DEALS']);
                    $sum = number_format($manager['DEAL_SUM'], 0, ',', ' ');
                    if ($num < 4)
                        $msg .= $num . ") {$manager['NAME']} (сделок:{$countDeals} , прибыль: {$sum} руб.) \n";
                    $num++;
                }
                $manager_bad = end($manager_list);
                $manager_bad_sum = number_format($manager_bad['DEAL_SUM'], 0, ',', ' ');
                $manager_bad_deals = count($manager_bad['DEAL_SUM']);
                $msg .= self::emoji('outer') . " Худший продавец:\n";
                $msg .= "{$manager_bad['NAME']} (сделок:{$manager_bad_deals}, прибыль: {$manager_bad_sum} руб.) \n";

                /*
                 *
                 * Топ 3 за сегодня:
                1. Иван Иванов (24 сделки, прибыль: 85 745 руб.)
                2. Иван Иванов (24 сделки, прибыль: 85 745 руб.)
                3. Иван Иванов (24 сделки, прибыль: 85 745 руб.)
                Худший продавец:
                Иван Иванов (2 сделки, прибыль: 15 745 руб.)
                 */

                break;

        }
        return $msg;
    }

    public static function getCompanyInfo()
    {

    }

    public static function b_getLeadList($domain, $auth, $filter_array)
    {
        $url = $domain . "/rest/crm.lead.list.json?auth={$auth}";
        $res = curl_get_contents($url, array(
            'filter' => $filter_array
        ));
        $arRes = json_decode($res, true);
        return $arRes;
    }

    public static function b_getDealList($domain, $auth, $filter_array)
    {
        $url = $domain . "/rest/crm.deal.list.json?auth={$auth}";
        $res = curl_get_contents($url, array(
            'filter' => $filter_array
        ));
        $arRes = json_decode($res, true);
        return $arRes['result'];
    }

    public static function b_getInvoiceList($domain, $auth, $filter_array)
    {
        $url = $domain . "/rest/crm.invoice.list.json?auth={$auth}";
        $res = curl_get_contents($url, array(
            'filter' => $filter_array
        ));
        $arRes = json_decode($res, true);
        return $arRes['result'];
    }

    public static function b_getList($domain, $auth, $list, $filter_array)
    {
        $url = $domain . "/rest/crm.{$list}.list.json?auth={$auth}";
        $res = curl_get_contents($url, array(
            'filter' => $filter_array
        ));
        $arRes = json_decode($res, true);
        return $arRes;
    }

    public static function b_getFields($domain, $auth, $field_name, $filter_array)
    {
        $url = $domain . "/rest/crm.status.list.json?auth={$auth}";
        $res = curl_get_contents($url, array(
            'filter' => $filter_array,
            // 'select' => array('PHONE')
        ));
        $arRes = json_decode($res, true);
        return $arRes['result'];
    }

    public static function b_getSourceList($domain, $auth)
    {
        $url = $domain . "/rest/crm.status.list.json?auth={$auth}";
        $res = curl_get_contents($url, array(
            'filter' => array('ENTITY_ID' => 'SOURCE')
        ));
        $arRes = json_decode($res, true);
        return $arRes['result'];
    }

    public static function b_getStatusList($domain, $auth)
    {
        $url = $domain . "/rest/crm.status.list.json?auth={$auth}";
        $res = curl_get_contents($url, array(
            'filter' => array('ENTITY_ID' => 'STATUS')
        ));
        $arRes = json_decode($res, true);
        return $arRes['result'];
    }

    public static function b_getStageList($domain, $auth)
    {
        $url = $domain . "/rest/crm.status.list.json?auth={$auth}";
        $res = curl_get_contents($url, array(
            'filter' => array('ENTITY_ID' => 'DEAL_STAGE')
        ));
        $arRes = json_decode($res, true);
        return $arRes['result'];
    }

    public static function checkAdmin($domain = false, $auth = false, $type = false)
    {
        if (!$domain) {
            $domain = $GLOBALS['DOMAIN'];
        }
        if (!$auth) {
            $auth = $GLOBALS['AUTH_ID'];
        }
        $url = $domain . '/rest/user.admin.json?auth=' . $auth;
        $res = curl_get_contents($url);
        $arRes = json_decode($res, true);
        if ($type) {
            return $arRes;
        }
        return $arRes['result'];
    }

    public static function getRefreshToken($client_id, $client_secret, $refresh_token)
    {
        //access_token - действует до expires
        //refresh_token - действует 28 дней
        //expires -- время действия access_token
        //2400000 27 дней

        $url = "https://oauth.bitrix.info/oauth/token/?grant_type=refresh_token&client_id={$client_id}&client_secret={$client_secret}&refresh_token={$refresh_token}";

        $res = curl_get_contents($url);
        $arRes = json_decode($res, true);
        return $arRes;
    }

    public static function b_getDepartsList($domain, $auth)
    {
        $url = $domain . "/rest/department.get.json?auth={$auth}";
        $res = curl_get_contents($url);
        $arRes = json_decode($res, true);
        return $arRes['result'];
    }

    public static function b_getUserList($domain, $auth, $filter)
    {
        $url = $domain . "/rest/user.get.json?auth={$auth}";
        $res = curl_get_contents($url, $filter);//array('UF_DEPARTMENT' => '5')
        $arRes = json_decode($res, true);
        return $arRes['result'];
    }

    public static function restCommand($command, $domain, $auth, $param)
    {
        $url = $domain . "/rest/{$command}.json?auth={$auth}";
        $res = curl_get_contents($url, $param);
        $arRes = json_decode($res, true);
        return $arRes;
    }


    public static function sendCronMessage($type)
    {
        switch ($type) {
            case 'b24_bot_crm':
                $functionListForNotify = b24_BitrixClass::getListFunctionsForNotify();
                foreach ($functionListForNotify as $function) {

                    $company = b24_BitrixClass::getCompany($function['PROP']['COMPANY']['VALUE']);
                    $company['PROP']['AUTH_ID']['VALUE'] = b24_BitrixClass::getTokenAuth($company);
                    $company_id = $company['ELEM']['ID'];

                    $chat_list = b24_BitrixClass::getChatListByCompany($company_id);

                    $domain = $company['ELEM']['NAME'];
                    $auth = b24_BitrixClass::getTokenAuth($company);
                    $checkAuth = self::checkAdmin($domain, $auth, 'Y');
                    if ($checkAuth['error'] == 'NO_AUTH_FOUND') {
                        foreach ($chat_list as $chat) {
                            $chat_id = $chat['ELEM']['NAME'];
                            //pre($chat_id.' '.$token);
                            self::restCommand('imbot.message.add', $domain, $auth, Array(
                                "DIALOG_ID" => $chat_id,
                                "MESSAGE" => 'Ваш бот неавторизован, пожалуйста, зайдите в настройки бота в своем Битрикс24.'
                            ));
                        }
                    } else {
                        $timeNow = new DateTime();
                        $timezoneClient = new DateTimeZone($company['PROP']['TIME_ZONE']['VALUE']);
                        $timeNow->setTimezone($timezoneClient);

                        $msg = $function['ELEM']['NAME'] . " за " . $timeNow->format('d.m.Y') . "\n\n";
                        switch ($function['ELEM']['NAME']) {
                            case 'Общий отчет':
                                $msg .= self::getMsgByTemplate($company, 'base');
                                break;
                            case 'Отчет по лидам':
                                $msg .= self::getMsgByTemplate($company, 'leads');
                                break;
                            case 'Отчет по сделкам':
                                $msg .= self::getMsgByTemplate($company, 'deals');
                                break;
                            case 'Отчет по продажам':
                                $msg .= self::getMsgByTemplate($company, 'sales');
                                break;
                            case 'Отчет по менеджерам':
                                $msg .= self::getMsgByTemplate($company, 'manager', array('DEPART' => $function['PROP']['DEPART_PARAM']['VALUE']));
                                break;
                        }
                        foreach ($chat_list as $chat) {
                            $chat_id = $chat['ELEM']['NAME'];
                            //pre($chat_id.' '.$token);
                            self::restCommand('imbot.message.add', $domain, $auth, Array(
                                "DIALOG_ID" => $chat_id,
                                "MESSAGE" => $msg
                            ));
                        }

                    }

                    b24_BitrixClass::setFunctionNotifyTime(
                        $function['ELEM']['ID'],
                        $function['PROP']['TIME']['VALUE'],
                        $company['PROP']['TIME_ZONE']['VALUE']
                    );

                }
            default;
        }
    }

    public static function emoji($name)
    {
        $code = '';
        switch ($name) {
            case 'push':
                if ($GLOBALS['msg_type'] == 'b24') {
                    $code = "[icon=https://app.kolibbri.ru/img/emoji/push.png size=18 title={$name}] ";
                } else {
                    $code = "\xF0\x9F\x93\x8C";
                }
                break;
            case 'deal':
                if ($GLOBALS['msg_type'] == 'b24') {
                    $code = "[icon=https://app.kolibbri.ru/img/emoji/deal.png size=18 title={$name}] ";
                } else {
                    $code = "\xF0\x9F\x91\x8F";
                }
                break;
            case 'check':
                if ($GLOBALS['msg_type'] == 'b24') {
                    $code = "[icon=https://app.kolibbri.ru/img/emoji/check.png size=18 title={$name}] ";
                } else {
                    $code = "\xE2\x9C\x85";
                }
                break;
            case 'money':
                if ($GLOBALS['msg_type'] == 'b24') {
                    $code = "[icon=https://app.kolibbri.ru/img/emoji/money.png size=18 title={$name}] ";
                } else {
                    $code = "\xF0\x9F\x92\xB0";
                }
                break;
            case 'no-check':
                if ($GLOBALS['msg_type'] == 'b24') {
                    $code = "[icon=https://app.kolibbri.ru/img/emoji/no-check.png size=18 title={$name}] ";
                } else {
                    $code = "\xE2\x9D\x8C";
                }
                break;
            case 'clock':
                if ($GLOBALS['msg_type'] == 'b24') {
                    $code = "[icon=https://app.kolibbri.ru/img/emoji/clock.png size=18 title={$name}] ";
                } else {
                    $code = "\xE2\x8F\xB0";
                }
                break;
            case 'time':
                if ($GLOBALS['msg_type'] == 'b24') {
                    $code = "[icon=https://app.kolibbri.ru/img/emoji/time.png size=18 title={$name}] ";
                } else {
                    $code = "\xE2\x8C\x9B";
                }
                break;
            case 'alert':
                if ($GLOBALS['msg_type'] == 'b24') {
                    $code = "[icon=https://app.kolibbri.ru/img/emoji/alert.png size=18 title={$name}] ";
                } else {
                    $code = "\xE2\x9A\xA0";
                }
                break;
            case 'attention':
                if ($GLOBALS['msg_type'] == 'b24') {
                    $code = "[icon=https://app.kolibbri.ru/img/emoji/attention.png size=18 title={$name}] ";
                } else {
                    $code = "\xE2\x9D\x8C";
                }
                break;
            case 'top':
                if ($GLOBALS['msg_type'] == 'b24') {
                    $code = "[icon=https://app.kolibbri.ru/img/emoji/top.png size=18 title={$name}] ";
                } else {
                    $code = "\xF0\x9F\x94\x9D";
                }
                break;
            case 'outer':
                if ($GLOBALS['msg_type'] == 'b24') {
                    $code = "[icon=https://app.kolibbri.ru/img/emoji/outer.png size=18 title={$name}] ";
                } else {
                    $code = "\xF0\x9F\x91\x8E";
                }
                break;
        }
        return $code;
    }

    public static function registerBot($company){
        $handlerBackUrl = 'https://app.kolibbri.ru/b24_bot/app/bot24hook.php?id='.$company['ELEM']['ID'];
        $auth = $GLOBALS['AUTH_ID'];
        $domain = $company['ELEM']['NAME'];

        $result_reg = Bitrix24Class::restCommand('imbot.register',$domain,$auth, Array(
            'CODE' => 'kolibbri_bot_crm',
            'TYPE' => 'B',
            'EVENT_MESSAGE_ADD' => $handlerBackUrl,
            'EVENT_WELCOME_MESSAGE' => $handlerBackUrl,
            'EVENT_BOT_DELETE' => $handlerBackUrl,
            'OPENLINE' => 'Y', // this flag only for Open Channel mode http://bitrix24.ru/~bot-itr
            'PROPERTIES' => Array(
                'NAME' => 'Отчеты в Боте',
                'COLOR' => 'GREEN',
                'EMAIL' => 'info@kolibbri.ru',
                'PERSONAL_BIRTHDAY' => '2016-03-11',
                'WORK_POSITION' => 'Bot',
                'PERSONAL_WWW' => 'https://kolibbri.ru',
                'PERSONAL_GENDER' => 'M',
                'PERSONAL_PHOTO' => base64_encode(file_get_contents($_SERVER["DOCUMENT_ROOT"].'/avatar_bot.jpg')),
            )
        ));
        Bitrix24Class::restCommand('imbot.command.register',$domain,$auth, Array(
            'BOT_ID' => $result_reg['result'],
            'COMMAND' => 'MainReport',
            'COMMON' => 'N',
            'HIDDEN' => 'N',
            'EXTRANET_SUPPORT' => 'N',
            'LANG' => Array(
                Array('LANGUAGE_ID' => 'ru', 'TITLE' => 'Общий отчет', 'PARAMS' => ''),
            ),
            'EVENT_COMMAND_ADD' => $handlerBackUrl,
        ));

        Bitrix24Class::restCommand('imbot.command.register',$domain,$auth, Array(
            'BOT_ID' => $result_reg['result'],
            'COMMAND' => 'LeadReport',
            'COMMON' => 'N',
            'HIDDEN' => 'N',
            'EXTRANET_SUPPORT' => 'N',
            'LANG' => Array(
                Array('LANGUAGE_ID' => 'ru', 'TITLE' => 'Отчет по лидам', 'PARAMS' => ''),
            ),
            'EVENT_COMMAND_ADD' => $handlerBackUrl,
        ));
        Bitrix24Class::restCommand('imbot.command.register',$domain,$auth, Array(
            'BOT_ID' => $result_reg['result'],
            'COMMAND' => 'DealReport',
            'COMMON' => 'N',
            'HIDDEN' => 'N',
            'EXTRANET_SUPPORT' => 'N',
            'LANG' => Array(
                Array('LANGUAGE_ID' => 'ru', 'TITLE' => 'Отчет по сделкам', 'PARAMS' => ''),
            ),
            'EVENT_COMMAND_ADD' => $handlerBackUrl,
        ));
        Bitrix24Class::restCommand('imbot.command.register',$domain,$auth, Array(
            'BOT_ID' => $result_reg['result'],
            'COMMAND' => 'SaleReport',
            'COMMON' => 'N',
            'HIDDEN' => 'N',
            'EXTRANET_SUPPORT' => 'N',
            'LANG' => Array(
                Array('LANGUAGE_ID' => 'ru', 'TITLE' => 'Отчет по продажам', 'PARAMS' => ''),
            ),
            'EVENT_COMMAND_ADD' => $handlerBackUrl,
        ));
        Bitrix24Class::restCommand('imbot.command.register',$domain,$auth, Array(
            'BOT_ID' => $result_reg['result'],
            'COMMAND' => 'ManagerReport',
            'COMMON' => 'N',
            'HIDDEN' => 'N',
            'EXTRANET_SUPPORT' => 'N',
            'LANG' => Array(
                Array('LANGUAGE_ID' => 'ru', 'TITLE' => 'Отчет по менеджерам', 'PARAMS' => ''),
            ),
            'EVENT_COMMAND_ADD' => $handlerBackUrl,
        ));
    }
}

/*
 * $res = curl_get_contents($domain.'/rest/user.current.json?auth='.$auth);
 *
*/