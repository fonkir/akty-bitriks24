<?php
/*
require($_SERVER["DOCUMENT_ROOT"]."/local/lib/BitrixClass.php");
require($_SERVER["DOCUMENT_ROOT"]."/local/lib/Bitrix24Class.php");
require($_SERVER["DOCUMENT_ROOT"]."/local/lib/TelegramBot.php");*/
CModule::IncludeModule("iblock");
//Подключение классов
CModule::AddAutoloadClasses(
    '', // не указываем имя модуля
    array(
        // ключ - имя класса, значение - путь относительно корня сайта к файлу с классом
        'autoload' => '/local/vendor/autoload.php',
        'AppClass' => '/local/lib/AppClass.php',
        'BitrixClass' => '/local/lib/BitrixClass.php',
        'Bitrix24Class' => '/local/lib/Bitrix24Class.php',
        'TelegramBot' => '/local/lib/TelegramBot.php',
        'LeadInfoClass' => '/local/lib/LeadInfo/LeadInfoClass.php',
        'b24_BitrixClass' => '/b24_bot/app/b24_BitrixClass.php',
    )
);

function pre($array){
    echo('<pre>'); print_r($array); echo('</pre>');
}

function curl_get_contents($url,$array = false)
{
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $url);

    if(isset($array) && $array){
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($array));
    }

    $data = curl_exec($ch);
    curl_close($ch);

    return $data;
}

function CronBot(){
    //TelegramBot::sendCronMessage();
    return 'CronBot();';
}