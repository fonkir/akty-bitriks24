</div>

<?

//jQuery
$asset->addJs(SITE_TEMPLATE_PATH."/lib/jquery/jquery-3.2.1.min.js");

//Bootstrap
$asset->addCss(SITE_TEMPLATE_PATH."/lib/bootstrap/css/bootstrap.min.css");
$asset->addJs(SITE_TEMPLATE_PATH."/lib/bootstrap/js/bootstrap.min.js");

//moment
$asset->addJs(SITE_TEMPLATE_PATH."/lib/moment/moment.min.js");
$asset->addJs(SITE_TEMPLATE_PATH."/lib/moment/moment-with-locales.min.js");

//webix
$asset->addJs(SITE_TEMPLATE_PATH."/lib/webix/webix.js");
$asset->addCss(SITE_TEMPLATE_PATH."/lib/webix/webix.css");

//DatePicker
$asset->addJs($tplPath."/lib/datepicker/jquery-ui.min.js");
$asset->addCss($tplPath."/lib/datepicker/jquery-bootstrap-datepicker.css");
$asset->addJs($tplPath."/lib/datepicker/jquery.ui.datepicker-ru.js");

//main
$asset->addJs(SITE_TEMPLATE_PATH."/js/app.js");
$asset->addJs(SITE_TEMPLATE_PATH."/lib/main.js");
?>
</body>
</html>