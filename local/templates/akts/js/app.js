var datatable;
var path;

var app = {
    invoice() {
        var dfd = new $.Deferred();
        var company=$('#company-list').val();
        if (company == '') {
            company = null;
        }
        BX24.callMethod(
            "crm.invoice.list",
            {
                "order": {"DATE_PAYED": "DESC"},
                "filter": {
                    'STATUS_ID': 'P',
                    '>DATE_MARKED': moment($("input[name=to]").datepicker('getDate')).endOf('day').format(),
                    '<DATE_MARKED': moment($("input[name=from]").datepicker('getDate')).endOf('day').format(),
                    'UF_COMPANY_ID': company
                },
                "select": ['*']
            },
            function (result) {
                if (result.error())
                    console.error(result.error());
                else {
                    dfd.resolve(result.data());
                }
            }
        );
        return dfd.promise();
    },
    list(d) {
        // console.log(d);
        datatable.clearAll();
        datatable.parse(d);
    },
    company() {
        BX24.callMethod(
            "crm.company.list",
            {
                select: ["ID", "TITLE"]
            },
            function (result) {
                if (result.error())
                    console.error(result.error());
                else {
                    let company = result.data();
                    if (company.length != 0) {
                        $(company).each(function (index, value) {
                            $('#company-list').append('<option value="' + value.ID + '">' + value.TITLE + '</option>');
                        })
                    }
                }
            }
        );
    }
}


var print = {
    act(data) {
        var w = window.open();
        $(w.document.head).html(this.drawStyles());
        $(w.document.body).html(this.drawPrinterBody(data));
        w.document.title = 'Акт № ' + data.ID + ' от ' + moment(data.DATE_MARKED).format("DD.MM.YYYY");
        w.print();
    },
    click(e) {
        var _this = this;
        var id = $(e).data('id');
        BX24.callMethod(
            "crm.invoice.get",
            {"id": id},
            function (result) {
                if (result.error()) {
                    console.error(result.error());
                } else {
                    _this.act(result.data())
                }
            }
        );
    },
    drawStyles() {
        var head = "";
        head += '<style>';
        head += '    html{width: 100%; background-color: gray;}';
        head += '    body{font: 400 13px/15px Arial; width: 19cm; margin: 0 auto; background-color: #fff; padding: 1cm;}';
        head += '    h1{font-size: 21px; margin: 10px 0;border-bottom: 2px solid;padding-bottom: 10px;}';
        head += '    .head__table tr td {padding:5px;}';
        head += '    .body__table {width:100%;border:2px solid black;}';
        head += '    .body__table thead tr th {text-align:center;border:1px solid;}';
        head += '    .body__table tbody tr td {border:1px solid;padding:2px;}';
        head += '    .body__table tfoot tr td {border:1px solid;padding:2px;}';
        head += '    .body__table .tright {text-align:right;}';
        head += '    .body__table .tcentr {text-align:center;}';
        head += '    p{margin: 0 0 15px 0}';
        head += '    small{font-size: 10px;}';
        head += '    table, tr, td{font: 400 13px/15px Arial}';
        head += '    thead{font-weight: bold}';
        head += '    td{border-right: none; border-bottom: none}';
        head += '    td.noborder-lr{border-left: none; border-right: none}';
        head += '    td.itogo{border-top: 1px solid #000; border-left: none; border-right: none; vertical-align: top; font-weight: bold;}';
        head += '    td.itogo{border-top: 1px solid #000; border-left: none; border-right: none; vertical-align: top; font-weight: bold;}';
        head += '    .sign{margin-top: 15px;}';
        head += '    .sign__table{width:100%;position:relative;}';
        head += '    .sign span {width: 150px;height: 10px; display: inline-block;border-bottom: 1px solid;}';
        head += '</style>';

        return head;
    },
    drawPrinterBody(d) {
        var body = '';

        var sign = '';
        var stamp = '';

        if ($('#stamp').prop('checked')) {
            sign = '<img style="position: absolute;width: 130px" src="https://app.kolibbri.ru/'+path+'/img/sign.png">';
            stamp = '<img style="position: absolute;width: 170px;bottom: -115px;" src="https://app.kolibbri.ru/'+path+'/img/stamp.png">';
        }

        var company = d.INVOICE_PROPERTIES.COMPANY + ', ИНН ' + d.INVOICE_PROPERTIES.INN + ', КПП ' + d.INVOICE_PROPERTIES.KPP + ', ' + d.INVOICE_PROPERTIES.COMPANY_ADR;
        var base = 'Счет № ' + d.ID + ' от ' + moment(d.DATE_INSERT).format("DD.MM.YYYY") + ' г.';


        body += '<h1>Акт № ' + d.ID + ' от ' + moment(d.DATE_MARKED).format("DD.MM.YYYY") + ' г.</h1>';
        body += '<table cellspacing="0" cellpadding="0" class="head__table">';
        body += '<tbody>';
        body += '<tr><td>Исполнитель:</td><td>ИП Воронин Д.А., ИНН 591202073808, 614051 г. Пермь ул. Уинская, 8, кв. 181,р/с 40802810129200000472,' +
            'ФИЛИАЛ «НИЖЕГОРОДСКИЙ» АО «АЛЬФА-БАНК»,\n' +
            'к/с 30101810200000000824,\n' +
            'БИК 042202824</td></tr>';
        body += '<tr><td>Заказчик:</td><td>' + company + '</td></tr>';
        body += '<tr><td>Основание:</td><td>' + base + '</td></tr>';
        body += '</tbody>';
        body += '</table>';
        body += '<br>';
        body += '<table cellspacing="0" cellpadding="0" class="body__table">';
        body += '<thead>';
        body += '<tr><th>№</th><th>Наименование работы (услуги)</th><th>Ед. изм.</th><th>Кол-во</th><th>Цена</th><th>Сумма</th></tr>';
        body += '</thead>';
        body += '<tbody>';
        var summ = 0;
        $(d.PRODUCT_ROWS).each(function (i, val) {
            i++;
            var q = parseFloat(val.QUANTITY).toFixed(0);
            var p = parseFloat(val.PRICE).toFixed(2);
            summ = summ + p * q;
            body += '<tr>' +
                '<td class="tcentr">' + i + '</td>' +
                '<td>' + val.PRODUCT_NAME + '</td>' +
                '<td class="tcentr">' + val.MEASURE_NAME + '</td>' +
                '<td class="tcentr">' + q + '</td>' +
                '<td class="tright">' + p + '</td>' +
                '<td class="tright">' + p * q + '</td>' +
                '</tr>';
        })
        body += '<tfoot>';
        body += '<tr>' +
            '<td colspan="2">Вышеперечисленные услуги выполнены полностью и в срок. Заказчик не имеет претензий по объему, качеству и срокам оказания услуг</td>' +
            '<td colspan="3" class="tright"><b>Итого</b></td>' +
            '<td class="tright"><b>'+summ+'</b></td>' +
            '</tr>';
        body += '</tfoot>';
        body += '</tbody>';
        body += '</table>';
        body += '<br><br><p style="text-align: center">Всего оказано услуг на сумму: '+summ+' руб. (без НДС)</p>';
        body += '<br><br>';
        body += '<table class="sign__table" cellspacing="0" cellpadding="0">';
        body += '<tbody>';
        body += '<tr>' +
            '<td>' + sign +
            '<b style="font-size: 15px">Исполнитель:</b><br><div class="sign"><span></span> Воронин Д.А.</div><br>' +
            stamp +
            'М.П.' +
            '</td>' +
            '<td><b style="font-size: 15px">Заказчик:</b><br><div class="sign"><span></span></div><br>М.П.</td>' +
            '</tr>';
        body += '</tbody>';
        body += '</table>';

        return body;
    }
}






