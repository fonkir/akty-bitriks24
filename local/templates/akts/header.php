<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
use Bitrix\Main\Page\Asset;
use Bitrix\Main\Application;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
global $USER;

$tplPath = SITE_TEMPLATE_PATH;
$sitePath = '/';
$curPage = $APPLICATION->GetCurPage(true);
$app = Application::getInstance();
$asset = Asset::getInstance();
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <title><?$APPLICATION->ShowTitle()?></title>
    <script type="text/javascript" src="//api.bitrix24.com/api/v1/"></script>
    <!-- import Babel JavaScript -->
    <?$APPLICATION->ShowHead();?>
</head>
<body>
<?$APPLICATION->ShowPanel();?>
<div class="app container-fluid">