$(document).ready(function () {
    path = $('#path').val();

    datatable = webix.ui({
        view: "datatable",
        container: "grid",
        height: 500,
        footer:true,
        columns: [
            {
                id: "DATE_MARKED", header: "Дата", width: 200, template: function (obj) {
                    return moment(obj.DATE_MARKED).format("DD.MM.YYYY");
                }
            },
            {id: "ORDER_TOPIC", header: "Наименование", fillspace: true},
            {id: "PRICE", header: "Сумма", width: 150, footer:{content:"summColumn" }},
            {id: "ID", header: "Скачать в PDF", width: 150, template: "<a onclick='print.click(this)' data-id='#ID#' href='javascript:void(0);'>Скачать</a>"}
        ],
        data: []
    });

    function isDate(value) {
        return value instanceof Date;
    }

    $("input[name=to]").datepicker({
        dateFormat: 'dd.mm.yy',
        maxDate: new Date(),
    }).datepicker("setDate", '-1m');

    $("input[name=from]").datepicker({
        dateFormat: 'dd.mm.yy',
        maxDate: new Date(),
    }).datepicker("setDate", new Date());

    app.company();
    app.invoice().done(app.list);
    // app.invoice();

    $('#filter').submit(function (event) {
        event.preventDefault();
        app.invoice().done(app.list);
        // app.invoice(filter);
    })
})